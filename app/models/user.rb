class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :favourites

  before_save :generate_auth_token

  def self.add_user(payload)
    create(payload)
  end

  def generate_auth_token
    self.auth_token = loop do
      token = Devise.friendly_token(200)
      break token unless User.find_by_auth_token(token)
    end
  end

  def add_favourite(favourite_number = { })
    favourites.find_or_create_by(favourite_number)
  end
end
