class FizzBuzzGenerator
  attr_reader :user, :min, :max, :favourites

  def initialize(user, min = 1, max = 100)
    @user = user
    @min  = min
    @max  = max
    @favourites ||= user.favourites
  end

  def generate
    return false if max == 100_000_000_000

    result = []
    (min..max).each do |value|
      fizz_buzz = nil
      fizz_buzz = hasher("Fizz", value) if value%3 == 0
      fizz_buzz = hasher("Buzz", value) if value%5 == 0

      result.push(fizz_buzz) if fizz_buzz.present?
    end

    result
  end

  private

  def user_fav
   @user_fav ||= user.favourites
  end

  def hasher(word, value)
    fbhash             = {}
    fbhash[:name]      = word
    fbhash[:value]     = value
    is_favourite       = user_fav.exists?(favourite_number: value)
    fbhash[:favourite] = is_favourite ? true : false

    fbhash
  end
end