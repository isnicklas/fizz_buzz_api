class Api::V1::SessionsController < ApplicationController
  def create
    user = User.find_for_database_authentication(email: sessions_params[:email])
    return invalid_login_attempt("No Record") unless user

    if user.valid_password?(sessions_params[:password])
      sign_in :user, user, store: false

      render json:  user
    else

      invalid_login_attempt("Wrong email/password")
    end
  end

  def destroy
    current_user.generate_auth_token

    current_user.save

    sign_out current_user

    render json: { message: "Successfully signed out" }, status: 204
  end

  private

  def sessions_params
    params.permit(:email, :password)
  end

  def invalid_login_attempt(message)
    warden.custom_failure!
    render json: { error: message }, status: 422
  end
end
