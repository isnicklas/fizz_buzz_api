class Api::V1::RegistrationsController < ApplicationController
  def create
    user = User.add_user(registration_params)

    if user.persisted?
      render json: user, status: 200
    else
      render json: { error: user.errors }, status: 422
    end
  end

  private

  def registration_params
    params.permit(:email, :password)
  end
end
