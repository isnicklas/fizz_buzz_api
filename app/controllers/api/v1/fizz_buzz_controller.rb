class Api::V1::FizzBuzzController < ApplicationController
  before_action :authenticate_with_token, only: :create

  def index
    if params[:min] && params[:max] && params[:page_size]
      min = params[:min].to_i
      max = params[:max].to_i

      fizzbuzz = FizzBuzzGenerator.new(current_user, min, max).generate

      numbers  = fizzbuzz.take(params[:page_size].to_i)

      render json: numbers, status: 200
    else
      numbers =  FizzBuzzGenerator.new(current_user).generate

      render json: numbers, status: 200
    end
  end

  def create
    return render json: 'No numbers', status: 422 if params[:favourite].nil?

    current_user.add_favourite(favourite_number: params[:favourite])

    render json: nil, status: 204
  end
end
