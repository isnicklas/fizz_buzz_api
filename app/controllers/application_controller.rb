class ApplicationController < ActionController::API
  include ActionController::StrongParameters

  def current_user
    token = request.headers['X-Access-Token']

    @current_user ||= User.find_by(auth_token: token) if token.present?
  end

  def authenticate_with_token
    render json: { errors: "Not authenticated" },
      status: :unauthorized unless current_user.present?
  end

  def user_signed_in?
    current_user.present?
  end
end
