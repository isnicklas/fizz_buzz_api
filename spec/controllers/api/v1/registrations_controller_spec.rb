require 'rails_helper'

RSpec.describe Api::V1::RegistrationsController, type: :controller do
  describe 'GET #create' do
    let(:payload) { { email: 'isaac@fizzbuzz.com', password: 'password' } }

    it 'returns http success' do
      get :create, payload

      expect(response).to have_http_status(:success)
    end

    it 'creates a new user' do
      expect { get :create, payload }.to change { User.count }.by(1)
    end
  end

end
