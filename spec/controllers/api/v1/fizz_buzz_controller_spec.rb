require 'rails_helper'

RSpec.describe Api::V1::FizzBuzzController, type: :controller do
  before do
    request.headers['X-Access-Token'] = create(:user).auth_token
  end

  describe 'Get#index' do
    it 'returns a success http status' do
      get :index

      expect(response).to have_http_status :success
    end

    it 'returns a response with the first object being a fizz and value 3' do
      get :index

      expect(JSON.parse(response.body).first)
        .to eq({ "name"=>"Fizz", "value"=>3, "favourite"=>false })
    end

    context 'when no page numbers or size are given' do
      it 'returns a page size of 47' do
        get :index

        expect(JSON.parse(response.body).count).to eq 47
      end

      it 'the last number is 100' do
        get :index

        expect(JSON.parse(response.body).last["value"]).to eq 100
      end
    end
  end

  describe 'Post#create' do
    let(:payload) do
      {
        favourite: 3
      }
    end

    it 'adds 1 favourite numbers to favourite model' do
      expect {post :create, payload }.to change { Favourite.count }.by(1)
    end
  end
end
