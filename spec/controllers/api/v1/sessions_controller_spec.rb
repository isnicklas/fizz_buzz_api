require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  before { create(:user, payload) }

  let(:payload) { { email: 'isaac@fizzbuzz.com', password: 'password000' } }

  describe "GET #create" do
    it "returns http success" do
      post :create, payload
      expect(response).to have_http_status(:success)
    end
  end
end
