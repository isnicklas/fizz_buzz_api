require 'rails_helper'

RSpec.describe FizzBuzzGenerator do
  describe '#initialize' do
    context 'when no parameters are passed in' do
      subject { described_class.new(create(:user)) }

      it 'returns 1 as the minimum number' do
        expect(subject.min).to be 1
      end

      it 'return 100 million as the maximum number' do
        expect(subject.max).to be 100
      end
    end
  end

  describe '#generate' do
    let(:user) { create(:user) }
    subject { described_class.new(user, 1, 6) }

    it 'return an array' do
      expect(subject.generate).to be_a_kind_of Array
    end

    it 'returns a collection of fizz buzz values' do
      expect(subject.generate).to eq(
        [
          { name: "Fizz", value: 3, favourite: false },
          { name: "Buzz", value: 5, favourite: false },
          { name: "Fizz", value: 6, favourite: false }
        ]
      )
    end
  end
end