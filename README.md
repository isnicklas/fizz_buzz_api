### FizzBuzz Api

#### How It Works

The api caters for the following;
- The creation of a User on `/api/v1/registrations`.

- Log in of a User on  `/api/v1/sessions`.

- Retrieval of a JSON of FizzBuzz numbers with user favourite numbers marked. This could be done on a get request to the `root url`.Pagination is possible with the following supplied `pageSize, min & max` parameters.

- It allows for a user's favourite number to be saved on `post to root url`.

The API interfaces with an angular app which provides the UI.