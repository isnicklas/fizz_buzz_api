class CreateFavourites < ActiveRecord::Migration
  def change
    create_table :favourites do |t|
      t.references :user
      t.integer :favourite_number

      t.timestamps null: false
    end
  end
end
